/**
 * Created by pengbang on 29.12.2016.
 */
/**
 * Created by pengbang on 21.12.2016.
 */
import {PASS_SESSIONLIST,RESET_SESSION} from '../actions'
import {getMaxEvolutionCount} from '../utils'


export default function (state = [], action) {

    switch (action.type) {
        case RESET_SESSION:
            return []
            break;
        case PASS_SESSIONLIST:
            console.log("reducer-evo-session")
            return [
                 ...JSON.parse(JSON.stringify(action.sessionList.filter(e=> e && getMaxEvolutionCount(e) > 0)))
            ]
            break;
        default : return state


    }

}
