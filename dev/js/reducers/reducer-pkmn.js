/**
 * Created by pengbang on 20.12.2016.
 */
import {PKMN_COUNT_UPDATE, PKMN_CANDY_UPDATE, RESET_SESSION} from '../actions'
import {SORT_BY_ID_ASCENDING} from '../utils'
const PKMNLIST = {
    "1": {
        "name_DE": "Bisasam",
        "name_EN": "Bulbasaur",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "2": {
        "name_DE": "Bisaknosp",
        "name_EN": "Ivysaur",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "3": {
        "name_DE": "Bisaflor",
        "name_EN": "Venusaur",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "4": {
        "name_DE": "Glumanda",
        "name_EN": "Charmander",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "5": {
        "name_DE": "Glutexo",
        "name_EN": "Charmeleon",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "6": {
        "name_DE": "Glurak",
        "name_EN": "Charizard",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "7": {
        "name_DE": "Schiggy",
        "name_EN": "Squirtle",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "8": {
        "name_DE": "Schillok",
        "name_EN": "Wartortle",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "9": {
        "name_DE": "Turtok",
        "name_EN": "Blastoise",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "10": {
        "name_DE": "Raupy",
        "name_EN": "Caterpie",
        "count": 1,
        "candy": 1,
        "cost": 12,
        "misc": {}, "firstTime": false
    },
    "11": {
        "name_DE": "Safcon",
        "name_EN": "Metapod",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "12": {
        "name_DE": "Smettbo",
        "name_EN": "Butterfree",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "13": {
        "name_DE": "Hornliu",
        "name_EN": "Weedle",
        "count": 1,
        "candy": 1,
        "cost": 12,
        "misc": {}, "firstTime": false
    },
    "14": {
        "name_DE": "Kokuna",
        "name_EN": "Kakuna",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "15": {
        "name_DE": "Bibor",
        "name_EN": "Beedrill",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "16": {
        "name_DE": "Taubsi",
        "name_EN": "Pidgey",
        "count": 1,
        "candy": 1,
        "cost": 12,
        "misc": {}, "firstTime": false
    },
    "17": {
        "name_DE": "Tauboga",
        "name_EN": "Pidgeotto",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "18": {
        "name_DE": "Tauboss",
        "name_EN": "Pidgeot",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "19": {
        "name_DE": "Rattfratz",
        "name_EN": "Rattata",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "20": {
        "name_DE": "Rattikarl",
        "name_EN": "Raticate",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "21": {
        "name_DE": "Habitak",
        "name_EN": "Spearow",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "22": {
        "name_DE": "Ibitak",
        "name_EN": "Fearow",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "23": {
        "name_DE": "Rettan",
        "name_EN": "Ekans",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "24": {
        "name_DE": "Arbok",
        "name_EN": "Arbok",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "25": {
        "name_DE": "Pikachu",
        "name_EN": "Pikachu",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "26": {
        "name_DE": "Raichu",
        "name_EN": "Raichu",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "27": {
        "name_DE": "Sandan",
        "name_EN": "Sandshrew",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "28": {
        "name_DE": "Sandamer",
        "name_EN": "Sandslash",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "29": {
        "name_DE": "Nidoran ♀",
        "name_EN": "Nidoran ♀",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "30": {
        "name_DE": "Nidorina",
        "name_EN": "Nidorina",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "31": {
        "name_DE": "Nidoqueen",
        "name_EN": "Nidoqueen",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "32": {
        "name_DE": "Nidoran ♂",
        "name_EN": "Nidoran ♂",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "33": {
        "name_DE": "Nidorino",
        "name_EN": "Nidorino",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "34": {
        "name_DE": "Nidoking",
        "name_EN": "Nidoking",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "35": {
        "name_DE": "Piepi",
        "name_EN": "Clefairy",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "36": {
        "name_DE": "Pixi",
        "name_EN": "Clefable",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "37": {
        "name_DE": "Vulpix",
        "name_EN": "Vulpix",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "38": {
        "name_DE": "Vulnona",
        "name_EN": "Ninetales",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "39": {
        "name_DE": "Pummeluff",
        "name_EN": "Jigglypuff",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "40": {
        "name_DE": "Knuddeluff",
        "name_EN": "Wigglytuff",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "41": {
        "name_DE": "Zubat",
        "name_EN": "Zubat",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "42": {
        "name_DE": "Golbat",
        "name_EN": "Golbat",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "43": {
        "name_DE": "Myrapla",
        "name_EN": "Oddish",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "44": {
        "name_DE": "Duflor",
        "name_EN": "Gloom",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "45": {
        "name_DE": "Giflor",
        "name_EN": "Vileplume",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "46": {
        "name_DE": "Paras",
        "name_EN": "Paras",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "47": {
        "name_DE": "Parasek",
        "name_EN": "Parasect",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "48": {
        "name_DE": "Bluzuk",
        "name_EN": "Venonat",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "49": {
        "name_DE": "Omot",
        "name_EN": "Venomoth",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "50": {
        "name_DE": "Digda",
        "name_EN": "Diglett",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "51": {
        "name_DE": "Digdri",
        "name_EN": "Dugtrio",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "52": {
        "name_DE": "Mauzi",
        "name_EN": "Meowth",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "53": {
        "name_DE": "Snobilikat",
        "name_EN": "Persian",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "54": {
        "name_DE": "Enton",
        "name_EN": "Psyduck",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "55": {
        "name_DE": "Entoron",
        "name_EN": "Golduck",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "56": {
        "name_DE": "Menki",
        "name_EN": "Mankey",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "57": {
        "name_DE": "Rasaff",
        "name_EN": "Primeape",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "58": {
        "name_DE": "Fukano",
        "name_EN": "Growlithe",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "59": {
        "name_DE": "Arkani",
        "name_EN": "Arcanine",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "60": {
        "name_DE": "Quapsel",
        "name_EN": "Poliwag",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "61": {
        "name_DE": "Quaputzi",
        "name_EN": "Poliwhirl",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "62": {
        "name_DE": "Quappo",
        "name_EN": "Poliwrath",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "63": {
        "name_DE": "Abra",
        "name_EN": "Abra",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "64": {
        "name_DE": "Kadabra",
        "name_EN": "Kadabra",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "65": {
        "name_DE": "Simsala",
        "name_EN": "Alakazam",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "66": {
        "name_DE": "Machollo",
        "name_EN": "Machop",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "67": {
        "name_DE": "Maschock",
        "name_EN": "Machoke",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "68": {
        "name_DE": "Machomei",
        "name_EN": "Machamp",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "69": {
        "name_DE": "Knofensa",
        "name_EN": "Bellsprout",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "70": {
        "name_DE": "Ultrigaria",
        "name_EN": "Weepinbell",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "71": {
        "name_DE": "Sarzenia",
        "name_EN": "Victreebel",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "72": {
        "name_DE": "Tentacha",
        "name_EN": "Tentacool",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "73": {
        "name_DE": "Tentoxa",
        "name_EN": "Tentacruel",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "74": {
        "name_DE": "Kleinstein",
        "name_EN": "Geodude",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "75": {
        "name_DE": "Georok",
        "name_EN": "Graveler",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "76": {
        "name_DE": "Geowaz",
        "name_EN": "Golem",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "77": {
        "name_DE": "Ponita",
        "name_EN": "Ponyta",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "78": {
        "name_DE": "Gallopa",
        "name_EN": "Rapidash",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "79": {
        "name_DE": "Flegmon",
        "name_EN": "Slowpoke",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "80": {
        "name_DE": "Lahmus",
        "name_EN": "Slowbro",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "81": {
        "name_DE": "Magnetilo",
        "name_EN": "Magnemite",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "82": {
        "name_DE": "Magneton",
        "name_EN": "Magneton",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "83": {
        "name_DE": "Porenta",
        "name_EN": "Farfetch�d",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "84": {
        "name_DE": "Dodu",
        "name_EN": "Doduo",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "85": {
        "name_DE": "Dodri",
        "name_EN": "Dodrio",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "86": {
        "name_DE": "Jurob",
        "name_EN": "Seel",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "87": {
        "name_DE": "Jugong",
        "name_EN": "Dewgong",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "88": {
        "name_DE": "Sleima",
        "name_EN": "Grimer",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "89": {
        "name_DE": "Sleimok",
        "name_EN": "Muk",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "90": {
        "name_DE": "Muschas",
        "name_EN": "Shellder",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "91": {
        "name_DE": "Austos",
        "name_EN": "Cloyster",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "92": {
        "name_DE": "Nebulak",
        "name_EN": "Gastly",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "93": {
        "name_DE": "Alpollo",
        "name_EN": "Haunter",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "94": {
        "name_DE": "Gengar",
        "name_EN": "Gengar",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "95": {
        "name_DE": "Onix",
        "name_EN": "Onix",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "96": {
        "name_DE": "Traumato",
        "name_EN": "Drowzee",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "97": {
        "name_DE": "Hypno",
        "name_EN": "Hypno",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "98": {
        "name_DE": "Krabby",
        "name_EN": "Krabby",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "99": {
        "name_DE": "Kingler",
        "name_EN": "Kingler",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "100": {
        "name_DE": "Voltobal",
        "name_EN": "Voltorb",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "101": {
        "name_DE": "Lektrobal",
        "name_EN": "Electrode",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "102": {
        "name_DE": "Owei",
        "name_EN": "Exeggcute",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "103": {
        "name_DE": "Kokowei",
        "name_EN": "Exeggutor",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "104": {
        "name_DE": "Tragosso",
        "name_EN": "Cubone",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "105": {
        "name_DE": "Knogga",
        "name_EN": "Marowak",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "106": {
        "name_DE": "Kicklee",
        "name_EN": "Hitmonlee",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "107": {
        "name_DE": "Nockchan",
        "name_EN": "Hitmonchan",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "108": {
        "name_DE": "Schlurp",
        "name_EN": "Lickitung",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "109": {
        "name_DE": "Smogon",
        "name_EN": "Koffing",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "110": {
        "name_DE": "Smogmog",
        "name_EN": "Weezing",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "111": {
        "name_DE": "Rihorn",
        "name_EN": "Rhyhorn",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "112": {
        "name_DE": "Rizeros",
        "name_EN": "Rhydon",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "113": {
        "name_DE": "Chaneira",
        "name_EN": "Chansey",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "114": {
        "name_DE": "Tangela",
        "name_EN": "Tangela",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "115": {
        "name_DE": "Kangama",
        "name_EN": "Kangaskhan",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "116": {
        "name_DE": "Seeper",
        "name_EN": "Horsea",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "117": {
        "name_DE": "Seemon",
        "name_EN": "Seadra",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "118": {
        "name_DE": "Goldini",
        "name_EN": "Goldeen",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "119": {
        "name_DE": "Golking",
        "name_EN": "Seaking",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "120": {
        "name_DE": "Sterndu",
        "name_EN": "Staryu",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "121": {
        "name_DE": "Starmie",
        "name_EN": "Starmie",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "122": {
        "name_DE": "Pantimos",
        "name_EN": "Mr. Mime",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "123": {
        "name_DE": "Sichlor",
        "name_EN": "Scyther",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "124": {
        "name_DE": "Rossana",
        "name_EN": "Jynx",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "125": {
        "name_DE": "Elektek",
        "name_EN": "Electabuzz",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "126": {
        "name_DE": "Magmar",
        "name_EN": "Magmar",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "127": {
        "name_DE": "Pinsir",
        "name_EN": "Pinsir",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "128": {
        "name_DE": "Tauros",
        "name_EN": "Tauros",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "129": {
        "name_DE": "Karpador",
        "name_EN": "Magikarp",
        "count": 1,
        "candy": 1,
        "cost": 400,
        "misc": {}, "firstTime": false
    },
    "130": {
        "name_DE": "Garados",
        "name_EN": "Gyarados",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "131": {
        "name_DE": "Lapras",
        "name_EN": "Lapras",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "132": {
        "name_DE": "Ditto",
        "name_EN": "Ditto",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "133": {
        "name_DE": "Evoli",
        "name_EN": "Eevee",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "134": {
        "name_DE": "Aquana",
        "name_EN": "Vaporeon",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "135": {
        "name_DE": "Blitza",
        "name_EN": "Jolteon",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "136": {
        "name_DE": "Flamara",
        "name_EN": "Flareon",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "137": {
        "name_DE": "Porygon",
        "name_EN": "Porygon",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "138": {
        "name_DE": "Amonitas",
        "name_EN": "Omanyte",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "139": {
        "name_DE": "Amoroso",
        "name_EN": "Omastar",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "140": {
        "name_DE": "Kabuto",
        "name_EN": "Kabuto",
        "count": 1,
        "candy": 1,
        "cost": 50,
        "misc": {}, "firstTime": false
    },
    "141": {
        "name_DE": "Kabutops",
        "name_EN": "Kabutops",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "142": {
        "name_DE": "Aerodactyl",
        "name_EN": "Aerodactyl",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "143": {
        "name_DE": "Relaxo",
        "name_EN": "Snorlax",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "144": {
        "name_DE": "Arktos",
        "name_EN": "Articuno",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "145": {
        "name_DE": "Zapdos",
        "name_EN": "Zapdos",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "146": {
        "name_DE": "Lavados",
        "name_EN": "Moltres",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "147": {
        "name_DE": "Dratini",
        "name_EN": "Dratini",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "148": {
        "name_DE": "Dragonir",
        "name_EN": "Dragonair",
        "count": 1,
        "candy": 1,
        "cost": 100,
        "misc": {}, "firstTime": false
    },
    "149": {
        "name_DE": "Dragoran",
        "name_EN": "Dragonite",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "150": {
        "name_DE": "Mewtu",
        "name_EN": "Mewtwo",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "151": {
        "name_DE": "Mew",
        "name_EN": "Mew",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "152": {
        "name_DE": "Endivie",
        "name_EN": "Chikorita",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "153": {
        "name_DE": "Lorblatt",
        "name_EN": "Bayleef",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "154": {
        "name_DE": "Meganie",
        "name_EN": "Meganium",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "155": {
        "name_DE": "Feurigel",
        "name_EN": "Cyndaquil",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "156": {
        "name_DE": "Igelavar",
        "name_EN": "Quilava",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "157": {
        "name_DE": "Tornupto",
        "name_EN": "Typhlosion",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "158": {
        "name_DE": "Karnimani",
        "name_EN": "Totodile",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "159": {
        "name_DE": "Tyracroc",
        "name_EN": "Croconaw",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "160": {
        "name_DE": "Impergator",
        "name_EN": "Feraligatr",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "161": {
        "name_DE": "Wiesor",
        "name_EN": "Sentret",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "162": {
        "name_DE": "Wiesenior",
        "name_EN": "Furret",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "163": {
        "name_DE": "Hoothoot",
        "name_EN": "Hoothoot",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "164": {
        "name_DE": "Noctuh",
        "name_EN": "Noctowl",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "165": {
        "name_DE": "Ledyba",
        "name_EN": "Ledyba",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "166": {
        "name_DE": "Ledian",
        "name_EN": "Ledian",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "167": {
        "name_DE": "Webarak",
        "name_EN": "Spinarak",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "168": {
        "name_DE": "Ariados",
        "name_EN": "Ariados",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "169": {
        "name_DE": "Iksbat",
        "name_EN": "Crobat",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "170": {
        "name_DE": "Lampi",
        "name_EN": "Chinchou",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "171": {
        "name_DE": "Lanturn",
        "name_EN": "Lanturn",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "172": {
        "name_DE": "Pichu",
        "name_EN": "Pichu",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "173": {
        "name_DE": "Pii",
        "name_EN": "Cleffa",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "174": {
        "name_DE": "Fluffeluff",
        "name_EN": "Igglybuff",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "175": {
        "name_DE": "Togepi",
        "name_EN": "Togepi",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "176": {
        "name_DE": "Togetic",
        "name_EN": "Togetic",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "177": {
        "name_DE": "Natu",
        "name_EN": "Natu",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "178": {
        "name_DE": "Xatu",
        "name_EN": "Xatu",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "179": {
        "name_DE": "Voltilamm",
        "name_EN": "Mareep",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "180": {
        "name_DE": "Waaty",
        "name_EN": "Flaaffy",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "181": {
        "name_DE": "Ampharos",
        "name_EN": "Ampharos",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "182": {
        "name_DE": "Blubella",
        "name_EN": "Bellossom",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "183": {
        "name_DE": "Marill",
        "name_EN": "Marill",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "184": {
        "name_DE": "Azumarill",
        "name_EN": "Azumarill",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "185": {
        "name_DE": "Mogelbaum",
        "name_EN": "Sudowoodo",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "186": {
        "name_DE": "Quaxo",
        "name_EN": "Politoed",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "187": {
        "name_DE": "Hoppspross",
        "name_EN": "Hoppip",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "188": {
        "name_DE": "Hubelupf",
        "name_EN": "Skiploom",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "189": {
        "name_DE": "Papungha",
        "name_EN": "Jumpluff",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "190": {
        "name_DE": "Griffel",
        "name_EN": "Aipom",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "191": {
        "name_DE": "Sonnkern",
        "name_EN": "Sunkern",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "192": {
        "name_DE": "Sonnflora",
        "name_EN": "Sunflora",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "193": {
        "name_DE": "Yanma",
        "name_EN": "Yanma",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "194": {
        "name_DE": "Felino",
        "name_EN": "Wooper",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "195": {
        "name_DE": "Morlord",
        "name_EN": "Quagsire",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "196": {
        "name_DE": "Psiana",
        "name_EN": "Espeon",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "197": {
        "name_DE": "Nachtara",
        "name_EN": "Umbreon",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "198": {
        "name_DE": "Kramurx",
        "name_EN": "Murkrow",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "199": {
        "name_DE": "Laschoking",
        "name_EN": "Slowking",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "200": {
        "name_DE": "Traunfugil",
        "name_EN": "Misdreavus",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "201": {
        "name_DE": "Icognito",
        "name_EN": "Unown",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "202": {
        "name_DE": "Woingenau",
        "name_EN": "Wobbuffet",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "203": {
        "name_DE": "Girafarig",
        "name_EN": "Girafarig",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "204": {
        "name_DE": "Tannza",
        "name_EN": "Pineco",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "205": {
        "name_DE": "Forstellka",
        "name_EN": "Forretress",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "206": {
        "name_DE": "Dummisel",
        "name_EN": "Dunsparce",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "207": {
        "name_DE": "Skorgla",
        "name_EN": "Gligar",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "208": {
        "name_DE": "Stahlos",
        "name_EN": "Steelix",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "209": {
        "name_DE": "Snubbull",
        "name_EN": "Snubbull",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "210": {
        "name_DE": "Granbull",
        "name_EN": "Granbull",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "211": {
        "name_DE": "Baldorfish",
        "name_EN": "Qwilfish",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "212": {
        "name_DE": "Scherox",
        "name_EN": "Scizor",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "213": {
        "name_DE": "Pottrott",
        "name_EN": "Shuckle",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "214": {
        "name_DE": "Skaraborn",
        "name_EN": "Heracross",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "215": {
        "name_DE": "Sniebel",
        "name_EN": "Sneasel",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "216": {
        "name_DE": "Teddiursa",
        "name_EN": "Teddiursa",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "217": {
        "name_DE": "Ursaring",
        "name_EN": "Ursaring",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "218": {
        "name_DE": "Schneckmag",
        "name_EN": "Slugma",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "219": {
        "name_DE": "Magcargo",
        "name_EN": "Magcargo",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "220": {
        "name_DE": "Quiekel",
        "name_EN": "Swinub",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "221": {
        "name_DE": "Keifel",
        "name_EN": "Piloswine",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "222": {
        "name_DE": "Corasonn",
        "name_EN": "Corsola",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "223": {
        "name_DE": "Remoraid",
        "name_EN": "Remoraid",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "224": {
        "name_DE": "Octillery",
        "name_EN": "Octillery",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "225": {
        "name_DE": "Botogel",
        "name_EN": "Delibird",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "226": {
        "name_DE": "Mantax",
        "name_EN": "Mantine",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "227": {
        "name_DE": "Panzaeron",
        "name_EN": "Skarmory",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "228": {
        "name_DE": "Hunduster",
        "name_EN": "Houndour",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "229": {
        "name_DE": "Hundemon",
        "name_EN": "Houndoom",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "230": {
        "name_DE": "Seedraking",
        "name_EN": "Kingdra",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "231": {
        "name_DE": "Phanpy",
        "name_EN": "Phanpy",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "232": {
        "name_DE": "Donphan",
        "name_EN": "Donphan",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "233": {
        "name_DE": "Porygon2",
        "name_EN": "Porygon2",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "234": {
        "name_DE": "Damhirplex",
        "name_EN": "Stantler",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "235": {
        "name_DE": "Farbeagle",
        "name_EN": "Smeargle",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "236": {
        "name_DE": "Rabauz",
        "name_EN": "Tyrogue",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "237": {
        "name_DE": "Kapoera",
        "name_EN": "Hitmontop",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "238": {
        "name_DE": "Kussilla",
        "name_EN": "Smoochum",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "239": {
        "name_DE": "Elekid",
        "name_EN": "Elekid",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "240": {
        "name_DE": "Magby",
        "name_EN": "Magby",
        "count": 1,
        "candy": 1,
        "cost": 25,
        "misc": {}, "firstTime": false
    },
    "241": {
        "name_DE": "Miltank",
        "name_EN": "Miltank",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "242": {
        "name_DE": "Heiteira",
        "name_EN": "Blissey",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "243": {
        "name_DE": "Raikou",
        "name_EN": "Raikou",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "244": {
        "name_DE": "Entei",
        "name_EN": "Entei",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "245": {
        "name_DE": "Suicune",
        "name_EN": "Suicune",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "246": {
        "name_DE": "Larvitar",
        "name_EN": "Larvitar",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "247": {
        "name_DE": "Pupitar",
        "name_EN": "Pupitar",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "248": {
        "name_DE": "Despotar",
        "name_EN": "Tyranitar",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "249": {
        "name_DE": "Lugia",
        "name_EN": "Lugia",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "250": {
        "name_DE": "Ho-Oh",
        "name_EN": "Ho-Oh",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    },
    "251": {
        "name_DE": "Celebi",
        "name_UK": "Celebi",
        "count": 1,
        "candy": 1,
        "cost": 0,
        "misc": {}, "firstTime": false
    }
}
let PKMNARRAY = []

for (let id in PKMNLIST) {
    PKMNARRAY.push({
        id,
        ...PKMNLIST[id]
    })
}
PKMNARRAY = [...PKMNARRAY.sort(SORT_BY_ID_ASCENDING)]


export default function (state = PKMNARRAY, action) {
    let pkmn = action.payload

    switch (action.type) {
        case RESET_SESSION :
            return [...state]
            break
        case PKMN_COUNT_UPDATE :
            return [
                ...state.slice(0, parseInt(pkmn.id) - 1),
                {...pkmn},
                ...state.slice(parseInt(pkmn.id))
            ]
            break;

        case PKMN_CANDY_UPDATE :
            return [
                ...state.slice(0, parseInt(pkmn.id) - 1),
                {...pkmn},
                ...state.slice(parseInt(pkmn.id))
            ]
            break;


        default :
            return [...state]
    }

    // return PKMNARRAY

}