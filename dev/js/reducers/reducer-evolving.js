/**
 * Created by pengbang on 29.12.2016.
 */
/**
 * Created by pengbang on 21.12.2016.
 */
import {TOGGLE_EVOLVING,RESET_SESSION,RESET_EVOLVING} from '../actions'
export default function (state = false, action) {

    switch (action.type) {
        case RESET_SESSION:
            return false
            break
        case RESET_EVOLVING:
            return false
            break
        case TOGGLE_EVOLVING:
            return !state
            break
        default : return state
    }

}
