/**
 * Created by pengbang on 29.12.2016.
 */
/**
 * Created by pengbang on 21.12.2016.
 */
import {TOGGLE_START,RESET_SESSION} from '../actions'
export default function (state = false, action) {

    switch (action.type) {
        case RESET_SESSION:
            return false
            break
        case TOGGLE_START:
            return !state
            break
        default : return state
    }

}
