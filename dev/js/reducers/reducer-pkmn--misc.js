/**
 * Created by pengbang on 04.01.2017.
 */
import {USER_INPUT_NOTE} from '../actions'

const initialState = {
    notes : ""
}
export default function (state = initialState,action) {

    switch (action.type){

        case USER_INPUT_NOTE:
            return {...state, notes:action.payload}
            break;
        default : return state
    }

}
