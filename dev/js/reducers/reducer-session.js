/**
 * Created by pengbang on 21.12.2016.
 */
import {PKMN_SELECTED, PKMN_REMOVE, PKMN_CANDY_UPDATE, PKMN_COUNT_UPDATE, RESET_SESSION} from '../actions'
import {ENABLE_FIRST_TIME, DISABLE_FIRST_TIME} from "../actions/index";
export default function (state = [], action) {
    let pkmn = action.payload
    let pos = state.length > 0 && action.payload && action.payload.id ? state.map(e => parseInt(e.id)).indexOf(parseInt(pkmn.id)) : 0

    switch (action.type) {
        case RESET_SESSION:
            return []
            break
        case PKMN_SELECTED:
            for (let i = 0; i < state.length; i++) {
                if (state[i].id == action.payload.id) return state
            }
            return [...state, action.payload]
            break;
        case PKMN_REMOVE :

            if (state.length === 1) return []

            return [
                ...state.slice(0, pos),
                ...state.slice(pos + 1)
            ]
            break
        case PKMN_COUNT_UPDATE :
            return [
                ...state.slice(0, pos),
                {...pkmn},
                ...state.slice(pos + 1)
            ]
            break;

        case PKMN_CANDY_UPDATE :
            // pos = state.map(e => parseInt(e.id)).indexOf(parseInt(pkmn.id))
            return [
                ...state.slice(0, pos),
                {...pkmn},
                ...state.slice(pos + 1)
            ]
            break;

        case ENABLE_FIRST_TIME :
            pkmn.firstTime = true
            return [
                ...state.slice(0, pos),
                {...pkmn},
                ...state.slice(pos+1)
            ]
            break
        case DISABLE_FIRST_TIME :
            pkmn.firstTime = false
            return [
                ...state.slice(0, pos),
                {...pkmn},
                ...state.slice(pos+1)
            ]
            break
        default :
            return state


    }

}
