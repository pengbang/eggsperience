/**
 * Created by pengbang on 31.12.2016.
 */
import {TOGGLE_SIDE_DRAWER} from '../actions'

export default function (state = false, action) {
    switch (action.type){
        case TOGGLE_SIDE_DRAWER :
            return !state
        break
        default :return state
    }
}