import {combineReducers} from 'redux';
import PkmnReducer from './reducer-pkmn'
import SessionReducer from './reducer-session'
import ActivePkmnReducer from "./reducer-active-pkmn"
import sessionCompletedReducer from './reducer-session_completed'
import evoList from './reducer-evo-session'
import started from './start-reducer'
import evolving from './reducer-evolving'
import open from './reducer-side-drawer'
import searchText from './reducer-text-search'
import evoPerSession from './reducer-number-evo-per-session'
import language from './reducer-language'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'
/*
 * We combine all reducers into a single object before updated data is dispatched (sent) to store
 * Your entire applications state (store) is just whatever gets returned from all your reducers
 * */

const allReducers = combineReducers({
    pkmn : PkmnReducer,
    activePkmn : ActivePkmnReducer,
    sessionList: SessionReducer,
    sessionCompletedList : sessionCompletedReducer,
    started,
    evoList,
    evolving,
    open,
    searchText,
    evoPerSession,
    language,
    routing : routerReducer,
});

export default allReducers
