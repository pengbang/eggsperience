/**
 * Created by pengbang on 03.01.2017.
 */
import  {CHANGE_NUMBER_EVO_PER_SESSION} from '../actions'



export default function (state = 80, action) {
    switch (action.type){
        case CHANGE_NUMBER_EVO_PER_SESSION:
            return 0+action.payload
        default: return state
    }
}