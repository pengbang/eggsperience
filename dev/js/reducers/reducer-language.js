/**
 * Created by pengbang on 03.01.2017.
 */
import {SWITCH_LANGUAGE} from '../actions'
import {DE,EN} from '../constants'

export default function (state = DE, action) {

    switch (action.type){
        case SWITCH_LANGUAGE :
            return action.payload
        break;
        default : return state
    }
}