import {
    Icon_Flag_DE,
    Icon_Flag_US
} from 'material-ui-country-flags';
import Icon_Flag_UK from './svg-icons/Icon_Flag_UK'
import React from 'react'

export const
    DE = "DE",
    EN = "EN",
    US = "US"
export const LANGUAGES = {EN,DE}

export const countryFlags = {
    Icon_Flag_US :( <Icon_Flag_US />),
    Icon_Flag_EN :( <Icon_Flag_UK />),
    Icon_Flag_DE :( <Icon_Flag_DE />)
}
export const MOST_COMMON =[{TAUBSI: 16}, {RATTFRATZ :19}, {RAUPY :10},{ HORNLIU :13}, {PARAS :46},{TRAUMATO :96}, {ZUBAT :41}, {HABITAK :21}]