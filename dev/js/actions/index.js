export const GET_PKMN_LIST = "GET_PKMN_LIST"
export const USER_SELECTED = "USER_SELECTED"
export const PKMN_SELECTED = "PKMN_SELECTED"
export const PKMN_COUNT_UPDATE = "PKMN_COUNT_UPDATE"
export const PKMN_CANDY_UPDATE = "PKMN_CANDY_UPDATE"
export const PKMN_REMOVE = "PKMN_REMOVE"
export const PKMN_COMPLETED = "PKMN_COMPLETED"
export const TOGGLE_START = "TOGGLE_START"
export const TOGGLE_EVOLVING = "TOGGLE_EVOLVING"
export const PASS_SESSIONLIST = "PASS_SESSIONLIST"
export const RESET_SESSION = "RESET_SESSION"
export const RESET_EVOLVING = "RESET_EVOLVING"
export const TOGGLE_SIDE_DRAWER = "TOGGLE_SIDE_DRAWER"
export const USER_INPUT_TEXT = "USER_INPUT_TEXT"
export const ENABLE_FIRST_TIME = "ENABLE_FIRST_TIME"
export const DISABLE_FIRST_TIME = "DISABLE_FIRST_TIME"
export const CHANGE_NUMBER_EVO_PER_SESSION = "CHANGE_NUMBER_EVO_PER_SESSION"
export const SWITCH_LANGUAGE = "SWITCH_LANGUAGE"
export const USER_INPUT_NOTE = "USER_INPUT_NOTE"



export const pkmnList = () =>{
    return {
        type : GET_PKMN_LIST
    }
}
export const selectPkmn = (pkmn) => {
    return {
        type: PKMN_SELECTED,
        payload : pkmn
    }
}
export const updateCount = (pkmn) => {
    return {
        type : PKMN_COUNT_UPDATE,
        payload : pkmn
    }
}
export const updateCandy = (pkmn) => {
    return {
        type : PKMN_CANDY_UPDATE,
        payload : pkmn
    }
}
export const removePkmn = (pkmn) => {
    return {
        type : PKMN_REMOVE,
        payload : pkmn
    }
}
export const completePkmn = pkmn => {
    return {
        type : PKMN_COMPLETED,
        payload : pkmn
    }
}
export const enableFirstTime = pkmn => {
    return {
        type : ENABLE_FIRST_TIME,
        payload : pkmn
    }
}
export const disableFirstTime = pkmn => {
    return {
        type : DISABLE_FIRST_TIME,
        payload : pkmn
    }
}
export const toggleStart = () => {
    return {
        type : TOGGLE_START
    }
}
export const toggleEvolving = () => {
    return {
        type : TOGGLE_EVOLVING
    }
}
export const passSessionList= sessionlist =>{
    return {
        type : PASS_SESSIONLIST,
        sessionList :sessionlist
    }
}
export const resetSession = () =>{
    return{
        type : RESET_SESSION
    }
}
export const resetEvolving = () =>{
    return{
        type: RESET_EVOLVING
    }
}
export const toggleSideDrawer = () =>{
    return{
        type: TOGGLE_SIDE_DRAWER
    }
}
export const userInputText = text => {
    return {
        type : USER_INPUT_TEXT,
        payload : text
    }
}
export const userInputNote = text => {
    return {
        type : USER_INPUT_NOTE,
        payload : text
    }
}
export const changeNumberEvoPerSession = num =>{
    return {
        type : CHANGE_NUMBER_EVO_PER_SESSION,
        payload : num
    }
}
export const switchLanguage = lang =>{
    return {
        type : SWITCH_LANGUAGE,
        payload : lang
    }
}