import 'babel-polyfill';
import React from 'react';
import ReactDOM from "react-dom";
import {Provider} from 'react-redux';
import App from './components/App';
import ImpComp from './components/ImpComp';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin'
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {store} from './store'

import { Router, Route, browserHistory, hashHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import DevTools from './DevTools';

let theme = store.getState().started ? getMuiTheme(darkBaseTheme) : null
injectTapEventPlugin();

const history = syncHistoryWithStore(hashHistory, store)
ReactDOM.render(
    <div>
    <Provider store={store}>
        <MuiThemeProvider muiTheme={theme}>
            <Router history={history} path="/">
                <Route path="/" component={App}/>
                <Route path="/impressum" component={ImpComp}/>

            </Router>

        </MuiThemeProvider>
    </Provider>
        <DevTools store={store}/>

    </div>,
    document.getElementById('root')
);
