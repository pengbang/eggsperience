/**
 * Created by pengbang on 31.12.2016.
 */
import {createStore, applyMiddleware} from 'redux';

import thunk from 'redux-thunk';
import promise from 'redux-promise';
import createLogger from 'redux-logger';
import allReducers from './reducers';
import DevTools from './DevTools';

const logger = createLogger();
export  const store = createStore(
    allReducers,
    DevTools.instrument(),
    applyMiddleware(
        thunk,
        promise,
        // logger
    )
);