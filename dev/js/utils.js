/**
 * Created by pengbang on 22.12.2016.
 */
export const SORT_BY_ID_ASCENDING = (a, b) => parseInt(a.id) - parseInt(b.id)
export const SORT_BY_ID_DESCENDING = (a, b) => parseInt(b.id) - parseInt(a.id)
export const toHHMMSS = function (num) {
    let sec_num = parseInt(num, 10); // don't forget the second param
    let hours = Math.floor(sec_num / 3600);
    let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return hours + ':' + minutes + ':' + seconds;
}

export const getAffortableEvolutions = pkmn => {
    const {count, candy, cost} = pkmn
    if (candy < cost || !count || !cost) return 0
    const maxCandyPool = candy +    Math.max(Math.floor(candy/cost),count-1)
    return  Math.floor(maxCandyPool/cost) //> count ? count :  Math.floor(maxCandyPool/cost)
}
export const getMaxEvolutionCount = pkmn => {
    const affortable = getAffortableEvolutions(pkmn)
    const {count} = pkmn
    return Math.min(affortable,count)
}

export const getTotalCost = pkmn => {
    const {cost, count} =pkmn
    return cost*count
}

export const getFirstTimeEvos = list => {
    return list.filter(e=> e.firstTime === true).length
}
export const getById =(list,id) => list.find(e=> e.id ==id)

export const getNumberPossibleEvo= (list)=>{
    return  list.map(e => Math.min(e.count,getMaxEvolutionCount(e))).reduce((p,n)=> +p+n,0)
}