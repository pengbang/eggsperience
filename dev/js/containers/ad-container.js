/**
 * Created by pengbang on 04.01.2017.
 */
import React from 'react';
import {grey400} from 'material-ui/styles/colors'
import {connect} from "react-redux";
class AdContainer extends React.Component {

    constructor(props){
        super(props)
    }
    componentDidMount() {

        (window.adsbygoogle = window.adsbygoogle || []).push({});
    }

    componentWillUnmount(){
        (window.adsbygoogle = []).push({})
    }
    render(){
        const style={
            width:"728px",
            height : "90px",
            position :"center"
        },
            flex = {
            paddingTop :"10px",
            display : "flex",
            justifyContent : "center"
            }

        return  <div style={flex}> <div style={style}>
            <ins className="adsbygoogle"
                  style={adStyle}
                  data-ad-client="ca-pub-2945829470739466"
                  data-ad-slot="3021193838"
                  type={ "image/text"|| "Document"}
            />
        </div> </div>
    }
}

const adStyle= {
    display:"inline-block",
    width:"728px",
    height:"90px"
}

const mapStateToProps = state =>({
    // started : state.started
})

export default connect(mapStateToProps)(AdContainer)