/**
 * Created by pengbang on 21.12.2016.
 */
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import React from 'react'
import {resetSession, resetEvolving,toggleSideDrawer,switchLanguage} from '../actions'
import AppBar from 'material-ui/AppBar'
import FlatButton from 'material-ui/FlatButton'
import {white} from 'material-ui/styles/colors'
import IconButton from 'material-ui/IconButton'
import LanguageSelector from './language-selector'
import {Link} from 'react-router'
import {DE,EN,countryFlags} from '../constants';

class Appbar extends React.Component {

    constructor(props) {
        super(props)
        this._onLanguageDe = this._onLanguageDe.bind(this)
        this._onLanguageEn = this._onLanguageEn.bind(this)
    }

    _onLanguageDe(){
        this.props.switchLanguage("de")
    }
    _onLanguageEn(){
        this.props.switchLanguage("en")
    }
    render() {
        const appbarStyle={
            paddingLeft: this.props.open ? "274px" :"20px",
            position : "fixed"

        }
        const style = {
            color: white
        }
        const titleStyle= {
            paddingBottom : "16px"
            }
        const btnStyle= {
            display: "flex",
            flexDirection: "row",
            alignItems : "center"
            },
            linkStyle={}
        return <AppBar
            style={appbarStyle}
            zDepth={2}
            title={<div>
                <Link to="/"  ><div style={style}> Eggsperience</div></Link>
            </div>}
            subTitle="test"
            showMenuIconButton={true}
            onLeftIconButtonTouchTap={()=>this.props.toggleSideDrawer()}
            iconElementRight={<span style={btnStyle}>
                {this.props.started
                && this.props.evolving
                &&
                <span><FlatButton label={this.props.language ==DE ? "| Entwicklung erlauben":this.props.language ==EN ? "| enable evolution" : "| enable evolution"} onTouchTap={() => this.props.resetEvolving()} style={style}/></span>}
                {(this.props.sessionList.length > 0 || this.props.sessionCompletedList.length > 0 ||this.props.started) &&<span> <FlatButton label={this.props.language ==DE ? "| Sitzung zurücksetzen":this.props.language ==EN ? "| reset session" : "| reset session"} onTouchTap={() => this.props.resetSession()} style={style}/></span>}
                <div>{countryFlags["Icon_Flag_"+this.props.language]}</div>
                <LanguageSelector/>
                </span>
            }

        />
    }

}
function mapStateToProps(state) {
    return {
        started: state.started,
        evolving: state.evolving,
        sessionList: state.sessionList,
        sessionCompletedList: state.sessionCompletedList,
        open: state.open,
        language: state.language,
        location : state.routing.location
    }
    };

function matchDispatchToProps(dispatch) {
    return bindActionCreators({resetEvolving, resetSession,toggleSideDrawer, switchLanguage}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Appbar);
