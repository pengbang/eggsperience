/**
 * Created by pengbang on 31.12.2016.
 */
import React from 'react';
import Drawer from 'material-ui/Drawer';
import {List,ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar'
import {bindActionCreators} from "redux";
import {toggleSideDrawer} from '../actions'
import {connect} from "react-redux";
import {URL} from '../../CONFIG'
import {DE,EN} from '../constants'
class SideDrawer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {open: false};
        this._getCompletedList = this._getCompletedList.bind(this);
    }
    _getCompletedList(){
        const {language,sessionCompletedList} = this.props
        return sessionCompletedList
        .reverse()
            .map(
            e=> <ListItem
                key={"evo_"+e["name_"+language]+Math.random()}
                primaryText={""+e["name_"+language]}
                leftAvatar={
                    <Avatar
                        src={`${URL}/img/${e.id}.png`}
                        size={30}
                    />
                }
                nestedItems={
                    Object.keys(e)
                        .map(key => <ListItem primaryText={ ""+key + " : "} secondaryText={""+e[key] }/>)
                }
            />)
    }


    render() {
        const h2style={
            paddingLeft : "20px"
        }
        const {language,open} = this.props
        const text = language ==EN ?
                "last evolved :" : language == DE ?
                "zuletzt entwickelt :" :  "last evolved :"
        return (
            <div>
                <Drawer open={open}>
                    <h2 style={h2style}>{text}</h2>
                    <List>
                        {this._getCompletedList()}
                    </List>
                </Drawer>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        started: state.started,
        open: state.open,
        sessionCompletedList: state.sessionCompletedList,
        language: state.language,
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({toggleSideDrawer}, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(SideDrawer);
