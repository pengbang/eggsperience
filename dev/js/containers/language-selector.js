/**
 * Created by pengbang on 03.01.2017.
 */
import React from 'react'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {switchLanguage} from '../actions'
import {white} from 'material-ui/styles/colors';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu  from 'material-ui/DropDownMenu';
import IconButton from 'material-ui/IconButton';
import {countryFlags,LANGUAGES} from '../constants'

class LanguageSelector extends React.Component {

    constructor(props){
        super(props)
        this._getLanguageMenuItems = this._getLanguageMenuItems.bind(this)
        this._handleChange = this._handleChange.bind(this)
    }
    _getLanguageMenuItems(){
        const style={
            fontWeight: "bold"
        }
        return Object.keys(LANGUAGES).map((lng) => <MenuItem innerDivStyle={style} key={lng} value={lng} leftIcon={countryFlags["Icon_Flag_"+lng]} primaryText={""+LANGUAGES[lng]}/>)
    }
    _handleChange(e,i,val){
        this.props.switchLanguage(val)
        // console.log()
    }

    render(){
        const style = {
            minWidth : "128px"
        }
        const labelStyle={
            color : white,
        }
        const {language} = this.props
        return  <DropDownMenu value={language}
                             style={style}

                              labelStyle={labelStyle}
                             autoWidth={true}
                              onChange={this._handleChange}
                              children= {this._getLanguageMenuItems()}
                />
    }
}



const mapStateToProps = (state) =>{
    return {
        language: state.language
    }

}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({switchLanguage}, dispatch);
}
export default connect(mapStateToProps,matchDispatchToProps)(LanguageSelector)