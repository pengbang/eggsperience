/**
 * Created by pengbang on 21.12.2016.
 */
import AutoComplete,{fuzzyFilter} from 'material-ui/AutoComplete'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import React from 'react'
import {selectPkmn,changeNumberEvoPerSession,passSessionList,userInputText,toggleStart,removePkmn} from '../actions'
import {getById,getMaxEvolutionCount} from '../utils'
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import StartSessionButton from '../components/StartSessionBtn'

import {DE,EN,MOST_COMMON} from '../constants'
import {getNumberPossibleEvo} from '../utils'

class PkmnSelector extends React.Component {

    constructor(props){
        super(props)
        this._onNewRequest = this._onNewRequest.bind(this)
        this._onNewNumberEvo = this._onNewNumberEvo.bind(this)
        this._onStartButton = this._onStartButton.bind(this)

    }
    _onNewRequest(e){
        if (e.cost === 0) return
        this.props.userInputText("")

        this.props.selectPkmn(e)

    }
    _onStartButton(){
        const {sessionList,passSessionList,toggleStart,removePkmn} = this.props
        sessionList.map(pkmn =>  getMaxEvolutionCount(pkmn) <1 && removePkmn(pkmn) )
        passSessionList(sessionList)
        toggleStart()

        this.forceUpdate()
    }
    _onNewNumberEvo(e){
        this.props.changeNumberEvoPerSession(parseInt(e.target.value))
    }
    render(){
        const most_common = MOST_COMMON
        const dataSourceConfig = {
            text : "name_"+this.props.language,
            value: "id"
            },divStyle = {
                marginLeft: this.props.open ? "256px" :"",
            },style={
            // borderRadius : "20px",
            fontWeight: "bold",
            fontSize : "18px",
            backgroundColor : "white",
            paddingLeft: "24px"
        },
            floatingLabelText = (()=>{
            switch (this.props.language) {
                case DE :
                    return "Wähle deine Pokemon...✔"

                break
                case EN :
                    return "Select your pokemon... ✔"
                break
                default : return "Select your pokemon... ✔"
            }
            })();
        const {sessionList,
            sessionCompletedList,
            started,
            pkmn,
            searchText,
            userInputText,
            evoPerSession
        } = this.props
        const numberEvolved = sessionCompletedList.length
        const total = numberEvolved + getNumberPossibleEvo(sessionList)


        return !this.props.started
            &&<div style={divStyle}> <AutoComplete
                floatingLabelText={floatingLabelText}
                filter={AutoComplete.fuzzyFilter }
                openOnFocus={true}
                dataSource={[...most_common.map((e,i,a) => getById(pkmn,e[Object.keys(e)[0]]) ),...pkmn]}
                dataSourceConfig={dataSourceConfig}
                fullWidth={false}
                maxSearchResults={pkmn.length}
                onNewRequest={this._onNewRequest}
                textFieldStyle={style}
                searchText={searchText}
                onUpdateInput={userInputText}

            />
                <TextField
                    type="number"
                    defaultValue={evoPerSession}
                    floatingLabelText={"# evolutions in 30min"}
                    onChange={this._onNewNumberEvo}
                />


            </div>
    }

}


function mapStateToProps(state) {
    return {
        pkmn : state.pkmn,
        started : state.started,
        open : state.open,
        searchText : state.searchText,
        evoPerSession : state.evoPerSession,
        language : state.language,
        sessionList : state.sessionList,
        sessionCompletedList : state.sessionCompletedList
    };
}

// Get actions and pass them as props to to UserList
//      > now UserList has this.props.selectUser
function matchDispatchToProps(dispatch){
    return bindActionCreators({selectPkmn: selectPkmn,removePkmn,changeNumberEvoPerSession,passSessionList,toggleStart,userInputText}, dispatch);
}

// We don't want to return the plain UserList (component) anymore, we want to return the smart Container
//      > UserList is now aware of state and actions
export default connect(mapStateToProps, matchDispatchToProps)(PkmnSelector);
