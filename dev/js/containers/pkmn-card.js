/**
 * Created by pengbang on 22.12.2016.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import { GridTile} from 'material-ui/GridList';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import Paper from 'material-ui/Paper';
import {updateCandy,selectPkmn,userInputNote,removePkmn,updateCount,completePkmn,toggleStart,toggleEvolving,enableFirstTime, disableFirstTime} from '../actions'
import {orange500, blue500, green500} from 'material-ui/styles/colors';
import {bindActionCreators} from "redux"
import TextField from 'material-ui/TextField';
import {getAffortableEvolutions,getMaxEvolutionCount} from '../utils'
import Checkbox from 'material-ui/Checkbox';
import IconButton from 'material-ui/IconButton';
import BackUpIcon from 'material-ui/svg-icons/editor/publish';
import RaisedButton from 'material-ui/RaisedButton';
import {EN,DE} from '../constants'


class PkmnCard extends Component {
    constructor(props){
        super(props)
        this._updateCount = this._updateCount.bind(this)
        this._updateCandy = this._updateCandy.bind(this)
        this._getTotalCost = this._getTotalCost.bind(this)
        this._onCardClick = this._onCardClick.bind(this)
        this._getOptimalCount = this._getOptimalCount.bind(this)
        this._countDown = this._countDown.bind(this)
        this._onCheckboxChange = this._onCheckboxChange.bind(this)
        this._updateNote = this._updateNote.bind(this)
    }
    _updateCount(event){
        let {current} = this.props
        if (event.target.value <= 0) return ;//this.props.removePkmn(current)
        current.count = parseInt(event.target.value)
        this.props.updateCount(current)
        this.forceUpdate()
    }
    _updateCandy(event){
        let {current} = this.props
        current.candy = parseInt(event.target.value)
        this.props.updateCandy(current)
        this.forceUpdate()

    }
    _updateNote(event,i,v){
        let {current, userInputNote} = this.props
        userInputNote(event.target.value)
        this.forceUpdate()

    }
    _getTotalCost(){
        // console.log("total cost",this.props.activePkmn.cost,this.props.activePkmn.count,this.props.activePkmn.count * +this.props.activePkmn.count)
        return this.props.cost * +this.props.count
    }
    _onCardClick(){
        this.props.selectPkmn(this.props.current)
        // if (this.props.started) this._countDown()
    }
    _getOptimalCount(){
        return Math.floor(this.props.candy/ this.props.cost ) ||0
        // return this._getOptimalCount() <= this.props.current.count
    }
    _countDown(){
        let curr= this.props.current
        if ( curr.candy < curr.cost){
            return
        }

        else if (curr.count <= 1){
                this.props.removePkmn(curr)


            // this.props.removePkmn(curr)

        }
        else {
            curr.count = curr.count-1
            curr.candy = curr.candy - curr.cost +1
            this.props.updateCandy(curr)
            this.props.updateCount(curr)
        }

        this.props.completePkmn(curr)
        if (this.props.sessionList.length  < 1){
            this.props.toggleStart()

        }
        this.props.toggleEvolving()
        if (curr.firstTime === true) this.props.disableFirstTime(curr)

        setTimeout(()=>this.props.toggleEvolving(), 18000)

        this.forceUpdate()
    }
    _onCheckboxChange(e){
        let {current,disableFirstTime,enableFirstTime} = this.props
        return e.target.checked ? enableFirstTime(current) : disableFirstTime(current)
    }
    render() {
        const errorStyleCount = {
            color: getAffortableEvolutions(this.props.current) <= this.props.current.count ? green500: orange500
        },
        errorStyleCandy = {
            color: this._getTotalCost()-this.props.current.count +1 <= this.props.current.candy ? green500: orange500
        },
        style = {
            width: "250px",
            // display :"flex"
            margin: "20px 10px",


        },
        inputStyle  ={
            fontWeight: "bold",
            // marginLeft : "14px",
            maxWidth : "250px"
        },
        flex = {
            display : "flex",
            flexDirection:  "row",
            justifyContent :"space-around"
        },
        floatingLabelStyle = {
            // marginLeft : "14px"

        },
        checkboxStyle = {
            // marginLeft : "14px",
            marginBottom : "4px",
            maxWidth : "250px"

        },
        checkboxLabelStyle = {
            color: "rgba(0, 0, 0, 0.298039)",
        },
            evoStyle = {
                backgroundColor : "transparent",
                position: "relative",
                display:"block",
                paddingTop: "0px",
                zDepth : "0",
                right: "0px",
            },
            overlayStyle={
                backgroundColor : "green"
            },
            overlayContainerStyle={
                backgroundColor : "blue",
                width : "100%",
            },
            btnStyle={
             // position :"absolute",
                alignSelf : "flex-end",
                width: "250px",
                right: "0",
                textAlign: "right",
                zDepth : "1",
                backgroundColor : "rgba(0,0,0,0.07)"
            },
            tooltipStyles = {
                marginLeft : "220px",
                zDepth : "0"
            }

        return (
        <Card style={style}
                  titlePosition="top"
                  children={(<Paper zDepth={0} width="250px">

                      {this.props.started ?
                          <IconButton tooltip={"evolve"}
                                      tooltipStyles={tooltipStyles}
                                      style={btnStyle}
                                      onTouchTap={this._countDown}
                                      disabled={this.props.evolving} ><BackUpIcon  color="black" /></IconButton>
                          :<span/>
                      }

                      <CardMedia overlay={<CardTitle title={'#'+this.props.current.id+" "+this.props.current["name_"+this.props.language]}
                              subtitle={this.props.language != EN && this.props.current.name_EN}
                          />}>


                      <img src={this.props["img-src"]}
                          alt={`ìmage of ${this.props.current["name_"+this.props.language]}, from http://bulbapedia.bulbagarden.net/`}
                      />




                    </CardMedia>
                      <TextField
                          disabled={this.props.started}
                          type="number"
                          name="pkmn-count__input"
                          floatingLabelText={this.props.language == DE ? "Anzahl :" : "Count"}
                          floatingLabelStyle={floatingLabelStyle}
                          errorStyle={errorStyleCount}
                          errorText={!this.props.started ? "optimal count by candy : "+getAffortableEvolutions(this.props.current) : `${this.props.sessionCompletedList.filter(e => e.id==this.props.current.id).length} from ${getMaxEvolutionCount(this.props.evoList.find(e=>e.id == this.props.current.id))} evolved`}
                          onChange={this._updateCount}
                          defaultValue={this.props.current.count}
                          style={inputStyle}

                      />
                      <TextField
                          disabled={this.props.started}
                          type="number"
                          name="pkmn-candy__input"
                          floatingLabelText={this.props.language == DE ? "Bonbons :" : "Candy :"}
                          floatingLabelStyle={floatingLabelStyle}
                          errorStyle={errorStyleCandy}
                          errorText={!this.props.started && "total cost : "+this._getTotalCost()+" required candy : "+(this._getTotalCost()-this.props.current.count +1)}
                          onChange={this._updateCandy}
                          defaultValue={this.props.current.candy}
                          style={inputStyle}

                      />
                      <TextField
                          type="text"
                          name="pkmn-note__input"
                          floatingLabelText={this.props.language == DE ? "Notizen :" : "Notes :"}
                          floatingLabelStyle={floatingLabelStyle}
                          onChange={this._updateNote}
                          multiLine={true}
                          defaultValue={this.props.current.misc["notes"] || ""}
                          style={inputStyle}
                          rows={2}

                      />

                      {
                          <Checkbox
                              style={checkboxStyle}
                              label="first evolution"
                              labelPosition="left"
                              labelStyle={checkboxLabelStyle}
                              onCheck={this._onCheckboxChange}
                              defaultChecked={this.props.current.firstTime}
                              disabled={this.props.started ||getAffortableEvolutions(this.props.current) <=0}
                          />}
                      <div>
                          {this.props.started && <RaisedButton label={this.props.evolving? "evolving...":"evolve"} disabled={this.props.current.candy < this.props.current.cost || this.props.evolving } primary={true} onTouchTap={this._countDown}/>}
                          {!this.props.started &&<RaisedButton label="remove" secondary={true} onTouchTap={()=>this.props.removePkmn(this.props.current)} />}
                      </div>
                  </Paper>)}
        />

        );
    }
}
PkmnCard.propTypes = {
    sessionList : React.PropTypes.array,
    started : React.PropTypes.bool
}
// "state.activeUser" is set in reducers/index.js
function mapStateToProps(state) {
    return {
        activePkmn : state.activePkmn,
        sessionList: state.sessionList,
        started : state.started,
        sessionCompletedList : state.sessionCompletedList,
        evoList : state.evoList,
        evolving : state.evolving,
        language : state.language,
    };
}
function matchDispatchToProps(dispatch){
    return bindActionCreators({ updateCandy,updateCount,selectPkmn,userInputNote,removePkmn,completePkmn,toggleStart,toggleEvolving,enableFirstTime,disableFirstTime}, dispatch);
}

export default connect(mapStateToProps,matchDispatchToProps)(PkmnCard);
