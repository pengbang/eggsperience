/**
 * Created by pengbang on 21.12.2016.
 */
import {connect} from 'react-redux'
import {Table, TableBody, TableRow, TableRowColumn} from 'material-ui/Table';
import {toHHMMSS,getFirstTimeEvos,getMaxEvolutionCount} from '../utils'
import {bindActionCreators} from 'redux'
import React from 'react'
import {toggleStart,passSessionList,removePkmn} from '../actions'
import RaisedButton from 'material-ui/RaisedButton';
import LinearProgress from 'material-ui/LinearProgress';
import StartSessionButton from '../components/StartSessionBtn'

let interval
class SessionOverview extends React.Component {

    constructor(props){
        super(props)
        this._onStartButton = this._onStartButton.bind(this)
        this._getNumberPossibleEvo = this._getNumberPossibleEvo.bind(this)
        this._getEstimatedTime = this._getEstimatedTime.bind(this)
        this.state = {
            time: 1800
        }
    }


    componentDidMount(){
       interval = setInterval(()=>{
            if (this.props.started) return this.setState({time : this.state.time- 1})
            return this.setState({time:1800})
        }
        ,1000)
    }

    _onStartButton(){
        this.props.sessionList.map(pkmn =>  getMaxEvolutionCount(pkmn) <1 && this.props.removePkmn(pkmn) )
        this.props.passSessionList(this.props.sessionList)
        this.props.toggleStart()

        this.forceUpdate()
    }
    _getNumberPossibleEvo(){
        return  this.props.sessionList.map(e => Math.min(e.count,getMaxEvolutionCount(e))).reduce((p,n)=> +p+n,0)
    }
    componentWillUnmount(){
        clearInterval(interval)
    }

    _getEstimatedTime(){
        // const numberOfFirstEvolutions = this.props.sessionList.map(e => e.firstTime).filter(e=> e === true).length
        return toHHMMSS((this._getNumberPossibleEvo()* (1800/this.props.evoPerSession)) + (2 * (this.props.sessionList.length > 1 ? this.props.sessionList.length-1 : 0)) + (6*getFirstTimeEvos(this.props.sessionList)))
    }
    render(){
        // const evolistTotal =  this.props.evoList.length > 0 ? this.props.evoList.map(e => e.count).reduce((p,n) => p+n) : 0
        // const numberToEvo = this.props.sessionList.length >0 ? this.props.sessionList.map(e => e.count).reduce((p,n)=> p+n) : 0
        // const numberPossibleEvo = this.props.sessionList > 0 ? this.props.sessionList.map(e => (e.cost * +e.count -e.count +1)).reduce((p,n)=> p+n) : 0
        const {sessionList,sessionCompletedList} = this.props
        const numberEvolved = sessionCompletedList.length
        const total = numberEvolved + this._getNumberPossibleEvo()
        const style=  {
            marginBottom: "10px",
            paddingLeft: this.props.open ? "256px" :"",
        },
            overview_EN= {
                maxEvo : "Total possible evolutions : ",
                totalEp : "Total EP : ",
                estTotalTime : "est. time :",
                remainingTime: "remaining LUCKY EGG time :",
                evoDone : "Evolutions done :",
                remainingEvo : "Remaining evolutions :",
                gainedEp :"EP gained "
            },
            overview_DE ={
                maxEvo : "Maximal mögliche Entwicklungen : ",
                totalEp : "Gesamt Erfahrung :",
                estTotalTime : "geschätzte Zeit :",
                remainingTime: "verbleibende GLÜCKS-EI dauer :",
                evoDone : "Fertig entwickelt :",
                remainingEvo : "verbleibende Entwicklungen :",
                gainedEp :"bisherige Erfahrungspunkte"
            },
            overview = {overview_DE,overview_EN}

        return <div style={style}>
            <Table>
                <TableBody displayRowCheckbox={false}>
                    <TableRow striped={true}>
                        <TableRowColumn><h3>{overview["overview_"+this.props.language].maxEvo}</h3></TableRowColumn>
                        <TableRowColumn><h3 className={this._getNumberPossibleEvo() < 75  ?"red": this._getNumberPossibleEvo() < this.props.evoPerSession ? "orange" : "green"}>{this.props.started? total : this._getNumberPossibleEvo()}</h3></TableRowColumn>
                    </TableRow>
                    <TableRow>
                        <TableRowColumn><h3>{overview["overview_"+this.props.language].totalEp}</h3></TableRowColumn>
                        <TableRowColumn><h3> {(total * 1000) + (getFirstTimeEvos(sessionList) * 1000) + (getFirstTimeEvos(sessionCompletedList)*1000)} EP</h3></TableRowColumn>
                    </TableRow>
                    <TableRow>
                        <TableRowColumn><h3>{overview["overview_"+this.props.language].estTotalTime}</h3></TableRowColumn>
                        <TableRowColumn><h3>{ this._getEstimatedTime()} </h3></TableRowColumn>
                    </TableRow>

                    {this.props.started &&
                    <TableRow>
                        <TableRowColumn className="green"><h3>{overview["overview_"+this.props.language].remainingTime}</h3></TableRowColumn>
                        <TableRowColumn className={this.state.time > 180 ? "green big" : "red big"}>{toHHMMSS(this.state.time) }</TableRowColumn>
                    </TableRow>
                    }
                    {this.props.started &&
                    <TableRow>
                        <TableRowColumn ><h3>{overview["overview_"+this.props.language].evoDone}</h3></TableRowColumn>
                        <TableRowColumn ><h3> { numberEvolved}</h3></TableRowColumn>
                    </TableRow>
                    }
                    {this.props.started &&
                    <TableRow>
                        <TableRowColumn ><h3>{overview["overview_"+this.props.language].remainingEvo}</h3></TableRowColumn>
                        <TableRowColumn ><h3> { total-  numberEvolved}</h3></TableRowColumn>
                    </TableRow>
                    }
                    {this.props.started &&
                     <TableRow>
                         <TableRowColumn ><h3>{overview["overview_"+this.props.language].gainedEp}</h3></TableRowColumn>
                         <TableRowColumn ><h3> {numberEvolved * 1000 + (this.props.sessionCompletedList.filter(e=> e.firstTime == true).length * 1000)} EP</h3></TableRowColumn>
                    </TableRow>
                    }
                </TableBody>
            </Table>

                {/*( sessionList.length > 0*/}
            {/*|| sessionCompletedList.length > 0)*/}
            {/*&& total > 0*/}
            {/*&&  */}
            <StartSessionButton/>
            {this.props.started && <LinearProgress mode="determinate" value={100/total *this.props.sessionCompletedList.length} />}


        </div>
    }

}
SessionOverview.PropTypes ={
    sessionList : React.PropTypes.array
}

function mapStateToProps(state) {
    return {
        sessionList : state.sessionList,
        started : state.started,
        evoList : state.evoList,
        sessionCompletedList : state.sessionCompletedList,
        open : state.open,
        evoPerSession : state.evoPerSession,
        language : state.language
    };
}

// Get actions and pass them as props to to UserList
//      > now UserList has this.props.selectUser
function matchDispatchToProps(dispatch){
    return bindActionCreators({toggleStart,passSessionList,removePkmn}, dispatch);
}

// We don't want to return the plain UserList (component) anymore, we want to return the smart Container
//      > UserList is now aware of state and actions
export default connect(mapStateToProps,matchDispatchToProps)(SessionOverview);
