/**
 * Created by pengbang on 04.01.2017.
 */

import React from 'react'
import {Link} from 'react-router'
import {BRAND} from '../../CONFIG'


const FooterComponent= () =>{
    const style= {
        width : "100%",
        height : "64px",
        backgroundColor: "rgba(0,0,0,0.42)",
        display: "flex",
        alignItems: "center",
        marginTop :"64px"


}
    return <div style={style}>
        <div className="footer__wrapper">
        <Link to="/">{BRAND}</Link>
        <Link to="/impressum">Impressum</Link>
        </div>
    </div>
}

export default FooterComponent