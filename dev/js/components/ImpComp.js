import React from 'react';
import PkmnSelector from '../containers/pkmn-selector';
import SessionComponent from '../components/SessionComponent';
import FooterComponent from '../components/FooterComponent';
import ImpressumComponent from '../components/ImpressumComponent';
import SessionOverview from '../containers/session-overview';
import AppBar from '../containers/app-bar';
import SideDrawer from '../containers/side-drawer';
import Paper from 'material-ui/Paper'
import AdComponent from '../containers/ad-container'
require('../../scss/style.scss');


const ImpComp = () => {

    const style={
        minHeight : "100vh"
    }
        ,bodyStyle= {
        display: "flex",
        flexDirection : "column",
        // textAlign: "center",
        justifyContent : "space-between",
        minHeight : "100%"

    }
    console.log('IMPRESSUM')
    return <Paper style={style} >
        <AppBar/>
        <div style={bodyStyle}>
            {ImpressumComponent()}
            {FooterComponent()}
        </div>
    </Paper>
};

export default ImpComp;
