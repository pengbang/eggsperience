import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import Paper from 'material-ui/Paper';

import {GridList, GridTile} from 'material-ui/GridList';
import {connect} from 'react-redux';
import {URL} from '../../CONFIG'
import {updateCount} from '../actions'
import PkmnCard from '../containers/pkmn-card'
import Card from 'material-ui/Card';
import {SORT_BY_ID_ASCENDING} from '../utils'
import {DE,EN} from '../constants'
import {green500} from 'material-ui/styles/colors';

/*
 * We need "if(!this.props.user)" because we set state to null by default
 * */

class SessionComponent extends Component {


    constructor(props){
        super(props)
        this._updatePkmn = this._updatePkmn.bind(this)
    }
    _updatePkmn(e){
      this.props.updatePkmn(e)
        this.forceUpdate()
    }
    render() {
        const style = {
            display :"flex",
            flexDirection: "row",
            flexWrap: 'wrap',
            overflowX: 'auto',
            justifyContent: "flex-start",
            marginRight: "20px",
            // alignContent : "flex-start",
            paddingLeft: this.props.open ? "280px" :"24px",
            minHeight: "100%",
            zDepth:"0",
            flexGrow : "99",
        },
            rootStyle = {
                // display: 'flex',
                // flexWrap: 'wrap',
                // justifyContent: 'space-around',
            },{language} = this.props
        if (!this.props.activePkmn ||this.props.sessionList.length == 0) {
            return (<h2 style={style}>{language==EN ?"select your pokemons to evolve at the top of the page..." : language ==DE? "Bitte wähle deine Pokemon über der Übersicht..." :  "select your pokemons to evolve at the top of the section..."}</h2>);
        }
        if (this.props.started){
            return (
                    <div style={style} >
                        {!this.props.started && this.props.evoList.length >0 ?this.props.evoList.sort(SORT_BY_ID_ASCENDING).map(pkmn=>

                                pkmn && pkmn["count"] >0 && <PkmnCard key={pkmn.id} img-src={`${URL}/img/${pkmn.id}.png`} current={pkmn} _updateProps={this.props.updateCount} candy={pkmn.candy} count={pkmn.count} cost={pkmn.cost}/>
                            ) :<div></div> }

                    </div>
                )
        }
        return (
                <div style={style} >
                        {!this.props.started && this.props.sessionList.length >0 ?this.props.sessionList.sort(SORT_BY_ID_ASCENDING).map(pkmn=>

                        pkmn && pkmn["count"] >0 && <PkmnCard key={pkmn.id} img-src={`${URL}/img/${pkmn.id}.png`} current={pkmn} _updateProps={this.props.updateCount} candy={pkmn.candy} count={pkmn.count} cost={pkmn.cost}/>
                        ) :<div></div> }

                </div>
            )
    }
}
SessionComponent.propTypes = {
    sessionList : React.PropTypes.array
}


// "state.activeUser" is set in reducers/index.js
function mapStateToProps(state) {
    return {
        activePkmn: state.activePkmn,
        sessionList: state.sessionList,
        evoList: state.evoList,
        open: state.open,
        language: state.language,
    };
}
function matchDispatchToProps(dispatch){
    return bindActionCreators({updateCount: updateCount}, dispatch);
}
export default connect(mapStateToProps,matchDispatchToProps)(SessionComponent);
