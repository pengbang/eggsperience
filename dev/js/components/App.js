import React from 'react';
import PkmnSelector from '../containers/pkmn-selector';
import SessionComponent from '../components/SessionComponent';
import FooterComponent from '../components/FooterComponent';
import SessionOverview from '../containers/session-overview';
import AppBar from '../containers/app-bar';
import SideDrawer from '../containers/side-drawer';
import Paper from 'material-ui/Paper'
import AdComponent from '../containers/ad-container'
require('../../scss/style.scss');


const App = () => {

        const style={
                minHeight : "100vh",
            display : "flex",
            flexDirection:"column",
            justifyContent: "space-between"
        }
        ,bodyStyle= {
                justifyContent : "flex-start",
                minHeight : "100%",
            paddingTop : "64px"

        },mainStyle={
                minHeight : "100%"
        }
        return <Paper style={style} >
                <div style={mainStyle}>
                        <AppBar/>
                        <SideDrawer/>
                        <div style={bodyStyle}>
                                <PkmnSelector />
                                <SessionOverview/>
                                <AdComponent/>
                                <SessionComponent />
                        </div>
                </div>
                {FooterComponent()}

        </Paper>
};

export default App;
