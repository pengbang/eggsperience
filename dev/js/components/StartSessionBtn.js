/**
 * Created by pengbang on 06.01.2017.
 */

import React from 'react'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {passSessionList,toggleStart,removePkmn} from '../actions'
import {getMaxEvolutionCount,getNumberPossibleEvo} from "../utils";
import RaisedButton from 'material-ui/RaisedButton'

class StartSessionButton extends React.Component {
    constructor(p){
        super(p)
        this._onStartButton = this._onStartButton.bind(this)
    }

    _onStartButton(){
        const {sessionList,passSessionList,toggleStart,removePkmn} = this.props
        sessionList.map(pkmn =>  getMaxEvolutionCount(pkmn) <1 && removePkmn(pkmn))
        passSessionList(sessionList)
        toggleStart()

        this.forceUpdate()
    }
    render(){
        const {started,sessionList,evoList} = this.props
        const btnLabel = started ? "Abort" : "Start"

        return <RaisedButton label={btnLabel}
                      onTouchTap={this._onStartButton}
                      primary={!started}
                      disabled={
                          getNumberPossibleEvo(sessionList) < 1 && getNumberPossibleEvo(evoList) <1}
                      secondary={started}/>
    }


}


const mapStateToProps = (state)=> {
    return {
        started : state.started,
        sessionList : state.sessionList,
        passSessionList : state.passSessionList,
        toggleStart:  state.toggleStart,
        evoList:  state.evoList,
        removePkmn : state.removePkmn
    }
}
const mapDispatchToProps = (dispatch) =>{
    return bindActionCreators({passSessionList,toggleStart,removePkmn}, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(StartSessionButton)