/**
 * Created by pengbang on 31.12.2016.
 */
import { combineEpics } from 'redux-observable';
import searchUsers from './searchUsers';

export default combineEpics(
    searchUsers
);